package dxhclasscore.classfile;

import dxhclasscore.classfile.attributes.AttributeInfo;
import dxhclasscore.node.Node;
import lsieun.utils.ByteDashboard;
import lsieun.utils.ByteUtils;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-15 15:59
 * @description：
 * u2             attributes_count;
 * attribute_info attributes[attributes_count];
 *
 * attribute_info {
 *     u2 attribute_name_index;
 *     u4 attribute_length;
 *     u1 info[attribute_length];
 * }
 */

public class Attributes extends Node {
    private final int attributes_count;
    private final AttributeInfo[] attributes;

    public Attributes(ByteDashboard byteDashboard,
                      ConstantPool constantPool) {
        byte[] attributes_count_bytes = byteDashboard.nextN(2);
        this.attributes_count = ByteUtils.bytesToInt(attributes_count_bytes);
        for (int i = 0; i < attributes_count; i++) {

        }
    }
}
