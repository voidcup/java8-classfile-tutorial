package dxhclasscore.classfile;

import dxhclasscore.node.Node;
import lsieun.utils.*;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-10 23:02
 * @description：
 * 类文件由单个classfile结构组成
 */

public final class ClassFile extends Node {
    //魔数
    private final MagicNumber magicNumber;
    //class文件版本信息
    private final ClassVersion classVersion;
    //常量池信息
    private final ConstantPool constantPool;
    //该类的基本信息
    private final ClassInfo classInfo;
    //

    public ClassFile(final ByteDashboard dashboard){
        //从魔术开始读取
        magicNumber = new MagicNumber(dashboard);
        //读取class文件版本信息
        classVersion = new ClassVersion(dashboard);
        //开始读取常量池
        constantPool = new ConstantPool(dashboard);
        //开始读取ClassInfo信息
        classInfo = new ClassInfo(dashboard);
    }

    @Override
    public String toString() {
        return "ClassFile{" +
                "magicNumber=" + magicNumber +
                ", classVersion=" + classVersion +
                ", constantPool=" + constantPool +
                ", classInfo=" + classInfo +
                '}';
    }

    public static void main(String[] args) {
        // 第一步，输入参数
        String relative_path = "sample/HelloWorld.class";
        // 第二步，读取数据
        String filepath = FileUtils.getFilePath(relative_path);
        byte[] bytes = ReadUtils.readByPath(filepath);
        //构造ByteDashboard
        ByteDashboard byteDashboard = new ByteDashboard(bytes);
        ClassFile classInfo = new ClassFile(byteDashboard);
        System.out.println(classInfo);
    }
}
