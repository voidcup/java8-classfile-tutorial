package dxhclasscore.classfile;

import dxhclasscore.node.Node;
import lsieun.cst.AccessConst;
import lsieun.utils.ByteDashboard;
import lsieun.utils.ByteUtils;

import java.util.Arrays;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-14 23:25
 * @description：
 * u2  access_flags;
 * u2  this_class;
 * u2  super_class;
 * u2  interfaces_count;
 * u2  interfaces[interfaces_count];
 */
public class ClassInfo extends Node {
    private final int access_flags;
    private final int this_class;
    private final int super_class;
    private final int interfaces_count;
    private final int interfaces[];

    @Override
    public String toString() {
        return "ClassInfo{" +
                "access_flags=" + AccessConst.getClassAccessFlagsString(access_flags) +
                ", this_class=" + this_class +
                ", super_class=" + super_class +
                ", interfaces_count=" + interfaces_count +
                ", interfaces=" + Arrays.toString(interfaces) +
                '}';
    }

    public ClassInfo(ByteDashboard byteDashboard){
        byte[] access_flags_bytes = byteDashboard.nextN(2);
        byte[] this_class_bytes = byteDashboard.nextN(2);
        byte[] super_class_bytes = byteDashboard.nextN(2);
        byte[] interfaces_count_bytes = byteDashboard.nextN(2);
        this.access_flags = ByteUtils.bytesToInt(access_flags_bytes);
        this.this_class = ByteUtils.bytesToInt(this_class_bytes);
        this.super_class = ByteUtils.bytesToInt(super_class_bytes);
        this.interfaces_count = ByteUtils.bytesToInt(interfaces_count_bytes);
        this.interfaces = new int[interfaces_count];
        byte[] concatenate = ByteUtils.concatenate(access_flags_bytes, this_class_bytes, super_class_bytes);
        concatenate = ByteUtils.concatenate(concatenate, interfaces_count_bytes);
        for (int i = 0; i < interfaces_count; i++) {
            byte[] interface_bytes = byteDashboard.nextN(2);
            interfaces[i] = ByteUtils.bytesToInt(interface_bytes);
            concatenate = ByteUtils.concatenate(concatenate,interface_bytes);
        }
        super.bytes = concatenate;
    }
}
