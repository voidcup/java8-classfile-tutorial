package dxhclasscore.classfile;

import dxhclasscore.node.Node;
import lsieun.utils.ByteDashboard;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-10 23:05
 * @description：
 *
 * class 文件版本
 * minor_version 次要版本 u2
 * major_version 主要版本 u2
 */

public class ClassVersion extends Node {
    private final int minor_version;
    private final int major_version;

    public ClassVersion(ByteDashboard byteDashboard){
        minor_version = byteDashboard.readUnsignedShort();
        major_version = byteDashboard.readUnsignedShort();
    }

    public int getMinor_version() {
        return minor_version;
    }

    public int getMajor_version() {
        return major_version;
    }

    @Override
    public String toString() {
        return "ClassVersion{" +
                "minor_version=" + minor_version +
                ", major_version=" + major_version +
                '}';
    }
}
