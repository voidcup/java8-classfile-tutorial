package dxhclasscore.classfile;

import dxhclasscore.classfile.cp.Constant;
import lsieun.cst.CPConst;
import lsieun.utils.ByteDashboard;
import lsieun.utils.ByteUtils;

import java.util.Arrays;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-10 22:46
 * @description：
 * 常量池
 * u2      constant_pool_count;
 * cp_info constant_pool[constant_pool_count-1];
 */

public class ConstantPool {
    //常量池个数
    private final int constant_pool_count;
    //常量池
    private final Constant[] constant_pool;

    public ConstantPool(ByteDashboard byteDashboard){
        byte[] count_bytes = byteDashboard.nextN(2);
        constant_pool_count = ByteUtils.bytesToInt(count_bytes);
        constant_pool = new Constant[constant_pool_count - 1];

        for (int i = 0; i < constant_pool.length; i++) {
            Constant item = Constant.readConstant(byteDashboard);
            item.index = i + 1;
            this.constant_pool[i] = item;
            /* Quote from the JVM specification:
             * "All eight byte constants take up two spots in the constant pool.
             * If this is the n'th byte in the constant pool, then the next item
             * will be numbered n+2"
             *
             * Thus we have to increment the index counter.
             */
            byte tag = item.tag;
            if ((tag == CPConst.CONSTANT_Double) || (tag == CPConst.CONSTANT_Long)) {
                i++;
            }
        }
    }

    public Constant getConstant(final int index) {
        if (index >= constant_pool_count || index < 0) {
            throw new RuntimeException("Invalid constant pool reference: " + index
                    + ". Constant pool size is: " + this.constant_pool_count);
        }
        return constant_pool[index];
    }

    public Constant getConstant(final int index, final byte tag) {
        Constant c = getConstant(index);
        if (c == null) {
            throw new RuntimeException("Constant pool at index " + index + " is null.");
        }
        if (c.tag != tag) {
            throw new RuntimeException("Expected class '" + CPConst.getConstantName(tag)
                    + "' at index " + index + " and got " + c);
        }
        return c;
    }

    public String getConstantString(final int index, final byte tag) {
        Constant constant = getConstant(index, tag);
        return constant.value;
    }

    @Override
    public String toString() {
        return "ConstantPool{" +
                "constant_pool_count=" + constant_pool_count +
                ",constant_pool="+ Arrays.toString(constant_pool)+
                '}';
    }
}
