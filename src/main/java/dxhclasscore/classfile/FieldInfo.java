package dxhclasscore.classfile;

import dxhclasscore.node.Node;
import lsieun.utils.ByteDashboard;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-15 15:54
 * @description：
 * field_info {
 *     u2             access_flags;
 *     u2             name_index;
 *     u2             descriptor_index;
 *     u2             attributes_count;
 *     attribute_info attributes[attributes_count];
 * }
 */

public class FieldInfo extends Node {
    private final int access_flags;
    private final int name_index;
    private final int descriptor_index;
    private final Attributes attributes;
    private final String name;
    private final String descriptor;

    public FieldInfo(ByteDashboard byteDashboard) {
    }
}
