package dxhclasscore.classfile;

import dxhclasscore.node.Node;
import lsieun.utils.ByteDashboard;
import lsieun.utils.ByteUtils;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-14 23:23
 * @description：
 *
 * u2             fields_count;
 * field_info     fields[fields_count];
 *
 * field_info {
 *     u2             access_flags;
 *     u2             name_index;
 *     u2             descriptor_index;
 *     u2             attributes_count;
 *     attribute_info attributes[attributes_count];
 * }
 */

public class Fields extends Node {
    private final int fields_count;
    private final FieldInfo[] fields;

    public Fields(ByteDashboard byteDashboard) {
        byte[] fields_count_bytes = byteDashboard.nextN(2);
        this.fields_count = ByteUtils.bytesToInt(fields_count_bytes);

    }
}
