package dxhclasscore.classfile;

import dxhclasscore.node.Node;
import lsieun.utils.ByteDashboard;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-10 22:37
 * @description：
 * 该magic提供标识class文件格式的魔数；它具有价值0xCAFEBABE。
 * 长度 u4
 */

public class MagicNumber extends Node {

    public MagicNumber(ByteDashboard byteDashboard){
        super.bytes = byteDashboard.nextN(4);
        super.value = hex();
    }

    public String getMagicNumber(){
        return super.value;
    }

    @Override
    public String toString() {
        return "MagicNumber{" +
                "value='" + value + '\'' +
                '}';
    }
}
