package dxhclasscore.classfile.attributes;

import dxhclasscore.classfile.ConstantPool;
import dxhclasscore.node.Node;
import lsieun.cst.CPConst;
import lsieun.utils.ByteDashboard;
import lsieun.utils.ByteUtils;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-15 16:01
 * @description：
 * attribute_info {
 *     u2 attribute_name_index;
 *     u4 attribute_length;
 *     u1 info[attribute_length];
 * }
 */

public class AttributeInfo extends Node {
    private final int attribute_name_index;
    private final int attribute_length;
    public final String name;

    public AttributeInfo(ByteDashboard byteDashboard, ConstantPool constantPool) {
        byte[] attribute_name_index_bytes = byteDashboard.nextN(2);
        byte[] attribute_length_bytes = byteDashboard.nextN(4);
        this.attribute_name_index = ByteUtils.bytesToInt(attribute_name_index_bytes);
        this.attribute_length = ByteUtils.bytesToInt(attribute_length_bytes);
        this.name = constantPool
                .getConstantString(attribute_name_index, CPConst.CONSTANT_Utf8);
    }
}
