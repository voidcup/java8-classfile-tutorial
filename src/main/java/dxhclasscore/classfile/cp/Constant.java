package dxhclasscore.classfile.cp;

import dxhclasscore.node.Node;
import lsieun.cst.CPConst;
import lsieun.utils.ByteDashboard;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-10 22:51
 * @description：
 * 常量对象
 * cp_info {
 *     u1 tag;
 *     u1 info[];
 * }
 * CONSTANT_Class	7
 * CONSTANT_Fieldref	9
 * CONSTANT_Methodref	10
 * CONSTANT_InterfaceMethodref	11
 * CONSTANT_String	8
 * CONSTANT_Integer	3
 * CONSTANT_Float	4
 * CONSTANT_Long	5
 * CONSTANT_Double	6
 * CONSTANT_NameAndType	12
 * CONSTANT_Utf8	1
 * CONSTANT_MethodHandle	15
 * CONSTANT_MethodType	16
 * CONSTANT_InvokeDynamic	18
 */

public abstract class Constant extends Node {
    //常量池标签
    public final byte tag;
    //指向常量池的位置索引
    public int index;

    @Override
    public String toString() {
        return "Constant{" +
                "tag=" + CPConst.getConstantName(tag) +
                ", index=" + index +
                ", value="+ value +
                '}';
    }

    public Constant(byte tag) {
        this.tag = tag;
    }

    public static Constant readConstant(final ByteDashboard bd) {
        final byte tag = bd.peek();

        switch (tag) {
            case CPConst.CONSTANT_Utf8:
                return new ConstantUtf8(bd);
            case CPConst.CONSTANT_Integer:
                return new ConstantInteger(bd);
            case CPConst.CONSTANT_Float:
                return new ConstantFloat(bd);
            case CPConst.CONSTANT_Long:
                return new ConstantLong(bd);
            case CPConst.CONSTANT_Double:
                return new ConstantDouble(bd);
            case CPConst.CONSTANT_Class:
                return new ConstantClass(bd);
            case CPConst.CONSTANT_String:
                return new ConstantString(bd);
            case CPConst.CONSTANT_Fieldref:
                return new ConstantFieldRef(bd);
            case CPConst.CONSTANT_Methodref:
                return new ConstantMethodRef(bd);
            case CPConst.CONSTANT_InterfaceMethodref:
                return new ConstantInterfaceMethodRef(bd);
            case CPConst.CONSTANT_NameAndType:
                return new ConstantNameAndType(bd);
            case CPConst.CONSTANT_MethodHandle:
                return new ConstantMethodHandle(bd);
            case CPConst.CONSTANT_MethodType:
                return new ConstantMethodType(bd);
//            case CPConst.CONSTANT_Dynamic:
//                return new ConstantDynamic(bd);
            case CPConst.CONSTANT_InvokeDynamic:
                return new ConstantInvokeDynamic(bd);
            default:
                throw new RuntimeException("Invalid byte tag in constant pool: " + tag);
        }
    }
}
