package dxhclasscore.classfile.cp;

import lsieun.cst.CPConst;
import lsieun.utils.ByteDashboard;
import lsieun.utils.ByteUtils;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-13 22:21
 * @description：
 *
 * CONSTANT_Class_info {
 *     u1 tag;
 *     u2 name_index;
 * }
 *
 */

public class ConstantClass extends Constant {

    private int name_index;

    public ConstantClass(ByteDashboard dashboard) {
        super(CPConst.CONSTANT_Class);
        byte[] tag_bytes = dashboard.nextN(1);
        byte[] name_index_bytes = dashboard.nextN(2);

        this.name_index = ByteUtils.bytesToInt(name_index_bytes);
        super.value = "#" + this.name_index;
        super.bytes = ByteUtils.concatenate(tag_bytes, name_index_bytes);
    }
}
