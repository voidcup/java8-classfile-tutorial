package dxhclasscore.classfile.cp;

import lsieun.cst.CPConst;
import lsieun.utils.ByteDashboard;
import lsieun.utils.ByteUtils;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-13 22:44
 * @description：
 * CONSTANT_Double_info {
 *     u1 tag;
 *     u4 high_bytes;
 *     u4 low_bytes;
 * }
 */

public class ConstantDouble extends Constant {

    private final Double double_val;

    public ConstantDouble(ByteDashboard dashboard) {
        super(CPConst.CONSTANT_Double);
        byte[] tag_bytes = dashboard.nextN(1);
        byte[] val_bytes = dashboard.nextN(8);
        this.double_val = ByteUtils.toDouble(val_bytes);
        super.value = Double.toString(double_val);
        super.bytes = ByteUtils.concatenate(tag_bytes,val_bytes);
    }
}
