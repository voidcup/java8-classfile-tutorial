package dxhclasscore.classfile.cp;

import lsieun.cst.CPConst;
import lsieun.utils.ByteDashboard;
import lsieun.utils.ByteUtils;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-13 22:30
 * @description：
 * CONSTANT_Fieldref_info {
 *     u1 tag;
 *     u2 class_index;
 *     u2 name_and_type_index;
 * }
 */

public class ConstantFieldRef extends Constant {

    private final int class_index;
    private final int name_and_type_index;

    public ConstantFieldRef(ByteDashboard bd) {
        super(CPConst.CONSTANT_Fieldref);

        byte[] tag_bytes = bd.nextN(1);
        byte[] class_index_bytes = bd.nextN(2);
        byte[] name_and_type_index_bytes = bd.nextN(2);
        this.class_index = ByteUtils.bytesToInt(class_index_bytes);
        this.name_and_type_index = ByteUtils.bytesToInt(name_and_type_index_bytes);
        super.value = "#" + class_index + ".#" + name_and_type_index;
        super.bytes = ByteUtils.concatenate(tag_bytes, class_index_bytes, name_and_type_index_bytes);
    }
}
