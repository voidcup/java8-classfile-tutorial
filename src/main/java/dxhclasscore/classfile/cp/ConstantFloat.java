package dxhclasscore.classfile.cp;

import lsieun.cst.CPConst;
import lsieun.utils.ByteDashboard;
import lsieun.utils.ByteUtils;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-13 22:49
 * @description：
 * CONSTANT_Float_info {
 *     u1 tag;
 *     u4 bytes;
 * }
 */

public class ConstantFloat extends Constant {

    private final Float float_val;

    public ConstantFloat(ByteDashboard byteDashboard) {
        super(CPConst.CONSTANT_Float);
        byte[] tag_bytes = byteDashboard.nextN(1);
        byte[] val_bytes = byteDashboard.nextN(4);
        this.float_val = ByteUtils.toFloat(val_bytes);
        super.value = Float.toString(this.float_val);
        super.bytes = ByteUtils.concatenate(tag_bytes,val_bytes);
    }
}
