package dxhclasscore.classfile.cp;

import lsieun.cst.CPConst;
import lsieun.utils.ByteDashboard;
import lsieun.utils.ByteUtils;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-13 22:43
 * @description：
 * CONSTANT_Integer_info {
 *     u1 tag;
 *     u4 bytes;
 * }
 */

public class ConstantInteger extends Constant {

    private final Integer int_val;

    public ConstantInteger(ByteDashboard byteDashboard) {
        super(CPConst.CONSTANT_Integer);
        byte[] tag_bytes = byteDashboard.nextN(1);
        byte[] int_bytes = byteDashboard.nextN(4);
        this.int_val = ByteUtils.bytesToInt(int_bytes);
        super.value = String.valueOf(this.int_val);
        super.bytes = ByteUtils.concatenate(tag_bytes, int_bytes);
    }
}
