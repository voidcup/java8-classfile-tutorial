package dxhclasscore.classfile.cp;

import lsieun.cst.CPConst;
import lsieun.utils.ByteDashboard;
import lsieun.utils.ByteUtils;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-13 22:31
 * @description：
 * CONSTANT_InterfaceMethodref_info {
 *     u1 tag;
 *     u2 class_index;
 *     u2 name_and_type_index;
 * }
 */

public class ConstantInterfaceMethodRef extends Constant {

    private final int class_index;
    private final int name_and_type_index;

    public ConstantInterfaceMethodRef(ByteDashboard bd) {
        super(CPConst.CONSTANT_InterfaceMethodref);
        byte[] tag_bytes = bd.nextN(1);
        byte[] class_index_bytes = bd.nextN(2);
        byte[] name_and_type_index_bytes = bd.nextN(2);
        this.class_index = ByteUtils.bytesToInt(class_index_bytes);
        this.name_and_type_index = ByteUtils.bytesToInt(name_and_type_index_bytes);
        super.value = "#"+class_index+".#"+name_and_type_index;
        super.bytes = ByteUtils.concatenate(tag_bytes,class_index_bytes,name_and_type_index_bytes);
    }
}
