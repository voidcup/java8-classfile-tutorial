package dxhclasscore.classfile.cp;

import lsieun.cst.CPConst;
import lsieun.utils.ByteDashboard;
import lsieun.utils.ByteUtils;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-13 22:45
 * @description：
 * CONSTANT_InvokeDynamic_info {
 *     u1 tag;
 *     u2 bootstrap_method_attr_index;
 *     u2 name_and_type_index;
 * }
 */

public class ConstantInvokeDynamic extends Constant {
    private final int bootstrap_method_attr_index;
    private final int name_and_type_index;
    public ConstantInvokeDynamic(ByteDashboard dashboard) {
        super(CPConst.CONSTANT_InvokeDynamic);
        byte[] tag_bytes = dashboard.nextN(1);
        byte[] bootstrap_method_attr_index_bytes = dashboard.nextN(2);
        byte[] name_and_type_index_bytes = dashboard.nextN(2);
        this.bootstrap_method_attr_index = ByteUtils.bytesToInt(bootstrap_method_attr_index_bytes);
        this.name_and_type_index = ByteUtils.bytesToInt(name_and_type_index_bytes);
        super.value = String.format("#%d:#%d", bootstrap_method_attr_index, name_and_type_index);
        super.bytes = ByteUtils.concatenate(tag_bytes, bootstrap_method_attr_index_bytes, name_and_type_index_bytes);
    }
}
