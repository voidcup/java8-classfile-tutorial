package dxhclasscore.classfile.cp;

import lsieun.cst.CPConst;
import lsieun.utils.ByteDashboard;
import lsieun.utils.ByteUtils;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-13 22:43
 * @description：
 * CONSTANT_Long_info {
 *     u1 tag;
 *     u4 high_bytes;
 *     u4 low_bytes;
 * }
 */

public class ConstantLong extends Constant {

    private final Long long_val;

    public ConstantLong(ByteDashboard byteDashboard) {
        super(CPConst.CONSTANT_Long);
        byte[] tag_bytes = byteDashboard.nextN(1);
        byte[] val_bytes = byteDashboard.nextN(8);
        long_val = ByteUtils.toLong(val_bytes);
        super.value = Long.toString(long_val);
        super.bytes = ByteUtils.concatenate(tag_bytes,val_bytes);
    }
}
