package dxhclasscore.classfile.cp;

import lsieun.cst.CPConst;
import lsieun.utils.ByteDashboard;
import lsieun.utils.ByteUtils;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-13 22:45
 * @description：
 * CONSTANT_MethodHandle_info {
 *     u1 tag;
 *     u1 reference_kind;
 *     u2 reference_index;
 * }
 */

public class ConstantMethodHandle extends Constant {
    private final int reference_kind;
    private final int reference_index;
    public ConstantMethodHandle(ByteDashboard dashboard) {
        super(CPConst.CONSTANT_MethodHandle);
        byte[] tag_bytes = dashboard.nextN(1);
        byte[] reference_kind_bytes = dashboard.nextN(1);
        byte[] reference_index_bytes = dashboard.nextN(2);
        this.reference_kind = ByteUtils.bytesToInt(reference_kind_bytes);
        this.reference_index = ByteUtils.bytesToInt(reference_index_bytes);

        super.value = "#" + reference_index;
        super.bytes = ByteUtils.concatenate(tag_bytes, reference_kind_bytes, reference_index_bytes);
    }
}
