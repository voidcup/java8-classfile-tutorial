package dxhclasscore.classfile.cp;

import lsieun.cst.CPConst;
import lsieun.utils.ByteDashboard;
import lsieun.utils.ByteUtils;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-13 22:45
 * @description：
 * CONSTANT_MethodType_info {
 *     u1 tag;
 *     u2 descriptor_index;
 * }
 */

public class ConstantMethodType extends Constant {

    private final int descriptor_index;

    public ConstantMethodType(ByteDashboard dashboard) {
        super(CPConst.CONSTANT_MethodType);
        byte[] tag_bytes = dashboard.nextN(1);
        byte[] descriptor_index_bytes = dashboard.nextN(2);
        this.descriptor_index = ByteUtils.bytesToInt(descriptor_index_bytes);
        super.value = "#"+descriptor_index;
        super.bytes = ByteUtils.concatenate(tag_bytes,descriptor_index_bytes);
    }
}
