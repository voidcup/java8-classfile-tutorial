package dxhclasscore.classfile.cp;

import lsieun.cst.CPConst;
import lsieun.utils.ByteDashboard;
import lsieun.utils.ByteUtils;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-13 22:44
 * @description：
 * CONSTANT_NameAndType_info {
 *     u1 tag;
 *     u2 name_index;
 *     u2 descriptor_index;
 * }
 */

public class ConstantNameAndType extends Constant {

    private final int name_index;
    private final int descriptor_index;
    public ConstantNameAndType(ByteDashboard dashboard) {
        super(CPConst.CONSTANT_NameAndType);
        byte[] tag_bytes = dashboard.nextN(1);
        byte[] name_index_bytes = dashboard.nextN(2);
        byte[] descriptor_index_bytes = dashboard.nextN(2);
        this.name_index = ByteUtils.bytesToInt(name_index_bytes);
        this.descriptor_index = ByteUtils.bytesToInt(descriptor_index_bytes);
        super.value = "#"+name_index+"#:"+descriptor_index;
        super.bytes = ByteUtils.concatenate(tag_bytes,name_index_bytes,descriptor_index_bytes);
    }
}
