package dxhclasscore.classfile.cp;

import lsieun.cst.CPConst;
import lsieun.utils.ByteDashboard;
import lsieun.utils.ByteUtils;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-13 22:43
 * @description：
 * CONSTANT_String_info {
 *     u1 tag;
 *     u2 string_index;
 * }
 */

public class ConstantString extends Constant {

    private final int string_index;
    public ConstantString(ByteDashboard bd) {
        super(CPConst.CONSTANT_String);
        byte[] tag_bytes = bd.nextN(1);
        byte[] string_index_bytes = bd.nextN(2);
        this.string_index = ByteUtils.bytesToInt(string_index_bytes);
        super.value = "#"+string_index;
        super.bytes = ByteUtils.concatenate(tag_bytes,string_index_bytes);
    }
}
