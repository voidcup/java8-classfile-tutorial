package dxhclasscore.classfile.cp;

import lsieun.cst.CPConst;
import lsieun.utils.ByteDashboard;
import lsieun.utils.ByteUtils;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-10 23:52
 * @description：
 * CONSTANT_Utf8_info {
 *     u1 tag;
 *     u2 length;
 *     u1 bytes[length];
 * }
 */

public class ConstantUtf8 extends Constant{
    private final int length;
    private final String utf8_val;

    public ConstantUtf8(ByteDashboard bd) {
        super(CPConst.CONSTANT_Utf8);

        byte[] tag_bytes = bd.nextN(1);
        byte[] length_bytes = bd.nextN(2);
        int length = ByteUtils.bytesToInt(length_bytes);
        byte[] utf8_bytes = bd.nextN(length);

        this.length = length;
        this.utf8_val = ByteUtils.toModifiedUtf8(utf8_bytes);
        super.value = utf8_val;
        super.bytes = ByteUtils.concatenate(tag_bytes, length_bytes, utf8_bytes);
    }
}
