package dxhclasscore.classfile.exception;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-15 16:21
 * @description：
 */

public class ClassException extends Exception{
    public ClassException() {
        super();
    }

    public ClassException(String message) {
        super(message);
    }

    public ClassException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClassException(Throwable cause) {
        super(cause);
    }

    protected ClassException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
