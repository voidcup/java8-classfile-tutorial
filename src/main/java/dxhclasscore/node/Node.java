package dxhclasscore.node;


import lsieun.utils.HexUtils;

import java.io.Serializable;

/**
 * @author dxh
 * @version 1.0.0
 * @date 2022-10-10 22:32
 * @description：
 * 描述ClassFile文件节点的一种数据结构
 */

public abstract class Node implements Serializable {
    public byte[] bytes;
    public String value;

    public String hex(){
        return HexUtils.toHex(this.bytes);
    }

    @Override
    public String toString() {
        return value;
    }
}
