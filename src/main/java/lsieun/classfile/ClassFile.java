package lsieun.classfile;

import lsieun.utils.ByteDashboard;
import lsieun.utils.FileUtils;
import lsieun.utils.ReadUtils;
import lsieun.vs.Visitor;

public final class ClassFile extends Node {
    public MagicNumber magic_number;
    public CompilerVersion compiler_version;
    public ConstantPool constant_pool;
    public ClassInfo class_info;
    public Fields fields;
    public Methods methods;
    public Attributes attributes;

    public ClassFile(ByteDashboard bd) {
        this.magic_number = new MagicNumber(bd);
        this.compiler_version = new CompilerVersion(bd);
        this.constant_pool = new ConstantPool(bd);
        this.class_info = new ClassInfo(bd);
        this.fields = new Fields(bd, constant_pool);
        this.methods = new Methods(bd, constant_pool);
        this.attributes = new Attributes(bd, constant_pool);
    }

    public void accept(Visitor v) {
        v.visitClassFile(this);
    }

    public static ClassFile parse(byte[] bytes) {
        ByteDashboard bd = new ByteDashboard(bytes);
        return new ClassFile(bd);
    }

    public static void main(String[] args) {
        // 第一步，输入参数
        String relative_path = "sample/HelloWorld.class";
        // 第二步，读取数据
        String filepath = FileUtils.getFilePath(relative_path);
        byte[] bytes = ReadUtils.readByPath(filepath);
        //构造ByteDashboard
        ByteDashboard byteDashboard = new ByteDashboard(bytes);
        dxhclasscore.classfile.ClassFile classInfo = new dxhclasscore.classfile.ClassFile(byteDashboard);
        System.out.println(classInfo);
    }
}
